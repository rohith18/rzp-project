import datetime
import copy
import requests
from openpyxl import load_workbook

from fetching_json_data import args, final_c_to_p_response_list, final_p_to_r_response_list

def removing_duplicates(list_name):
    return list(set(list_name))

def extract_cname(in_list):
    out_list = []
    headers = {"session-token": "wantednote"}
    
    for cid in in_list:
        url = f"https://api.pharmacyone.io/prod/company_details/{cid}"
        response = requests.get(url, headers=headers)
        response_body = response.json()
        try:
            if response_body.get("data").get("Company").get("id"):
                out_list.append(response_body.get("data").get("Company").get("name"))
        except AttributeError:
            out_list.append("")
        
    return out_list

def unix_timestamp_to_date(in_list):
    out_list = []
    for item in in_list:
        value = datetime.datetime.fromtimestamp(item)
        out_list.append(f"{value:%Y/%m/%d %H:%M:%S}")

    return out_list

cust_to_onep_list = copy.deepcopy(final_c_to_p_response_list)
onep_to_ret_list = copy.deepcopy(final_p_to_r_response_list)

cust_to_onep_list_2, onep_to_ret_list_2, cust_to_onep_ids, onep_to_ret_ids, pay_list, payout_list, utr_list, amount_list, cid_list, cname_list, created_at_list = [[] for _ in range(11)]
matching_count, total_transaction_value = 0, 0
unmatched_pay_list, unmatched_payout_list, unmatched_created_at_list, unmatched_cname_list, unmatched_cid_list, unmatched_amount_list, unmatched_utr_list = [[] for _ in range(7)]
unmatched_created_at_list_2, unmatched_cname_list_2, unmatched_cid_list_2, unmatched_amount_list_2, created_at_list_formatted, unmatched_created_at_list_formatted, unmatched_created_at_list_2_formatted = [[] for _ in range(7)]

# Adding only valid ids to corresponding lists

for item in cust_to_onep_list:
    try:
        if item.get("id"):
            cust_to_onep_ids.append(item.get("id"))
            cust_to_onep_list_2.append(item)
    except AttributeError:
        pass

for item in onep_to_ret_list:
    try:
        if (item.get("source").get("notes").get("id") and item.get("source").get("status") == "processed"):
            onep_to_ret_ids.append(item.get("source").get("notes").get("id"))
            onep_to_ret_list_2.append(item)
    except AttributeError:
        pass

# Removing duplicates from ids lists 

cust_to_onep_ids = copy.deepcopy(removing_duplicates(cust_to_onep_ids))
onep_to_ret_ids = copy.deepcopy(removing_duplicates(onep_to_ret_ids))

unmatched_pay_list = copy.deepcopy(cust_to_onep_ids)
unmatched_payout_list = copy.deepcopy(onep_to_ret_ids)

# Checking for Matches between razr_transaction and razr_payout

for item_01 in cust_to_onep_ids:
    if item_01 in onep_to_ret_ids:
        matching_count += 1

        unmatched_pay_list.remove(item_01)
        unmatched_payout_list.remove(item_01)

        for item in onep_to_ret_list_2:
            if item_01 == item.get("source").get("notes").get("id"):
                pay_list.append(item_01)
                payout_list.append(item_01)
                utr_list.append(item.get("source").get("utr"))
                amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                cid_list.append(item.get("source").get("notes").get("cid"))
                created_at_list.append(item.get("created_at"))
                total_transaction_value += item.get("amount")

# Extracting the unmatched_ids information

for item_01 in unmatched_payout_list:
    for item in onep_to_ret_list_2:
        if item_01 == item.get("source").get("notes").get("id"):
            try:
                unmatched_utr_list.append(item.get("source").get("utr"))
                unmatched_amount_list.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                unmatched_cid_list.append(item.get("source").get("notes").get("cid"))
                unmatched_created_at_list.append(item.get("created_at"))
            except AttributeError:
                pass

for item_01 in unmatched_pay_list:
    for item in cust_to_onep_list_2:
        if item_01 == item.get("id"):
            try:
                unmatched_amount_list_2.append(int(item.get("amount")/ 100)) # Dividing by 100 to convert paisa into rupees
                unmatched_cid_list_2.append(item.get("notes").get("cid"))
                unmatched_created_at_list_2.append(item.get("created_at"))
            except AttributeError:
                pass

print("Count of Payments done by Customer to 1Pharmacy: ", len(cust_to_onep_ids))
print("Count of Payments done by 1Pharmacy to Retailer: ", len(onep_to_ret_ids))

print("Matching Payments Count: ", matching_count)
print("Total Transaction Value (in Rupees): ", int(total_transaction_value/ 100)) # Dividing by 100 to convert paisa into rupees

# Getting company_name for corresponding company_ids (cid)

cname_list = copy.deepcopy(extract_cname(cid_list))
unmatched_cname_list = copy.deepcopy(extract_cname(unmatched_cid_list))
unmatched_cname_list_2 = copy.deepcopy(extract_cname(unmatched_cid_list_2))

# Converting created_at unix timestamp to human readable format

created_at_list_formatted = copy.deepcopy(unix_timestamp_to_date(created_at_list))
unmatched_created_at_list_formatted = copy.deepcopy(unix_timestamp_to_date(unmatched_created_at_list))
unmatched_created_at_list_2_formatted = copy.deepcopy(unix_timestamp_to_date(unmatched_created_at_list_2))

# Writing data to excel

workbook = load_workbook("razor_pay_ids.xlsx")

summary_sheet = workbook["summary"]
summary_sheet.append([f"March{args.day}", int(total_transaction_value/ 100)])

if f"March{args.day}" in workbook.sheetnames:
    raise Exception("Sheet already exists")

sheet = workbook.create_sheet(f"March{args.day}")

sheet.append(["p_id", "pout_id", "utr", "amount (in Rupees)", "cid", "cname", "created_at"])

for i in range(len(pay_list)):
    sheet.append([pay_list[i], payout_list[i], utr_list[i], amount_list[i], cid_list[i], cname_list[i], created_at_list_formatted[i]])

for i in range(len(unmatched_pay_list)):
    sheet.append([unmatched_pay_list[i], "", "", unmatched_amount_list_2[i], unmatched_cid_list_2[i], unmatched_cname_list_2[i], unmatched_created_at_list_2_formatted[i]])

for i in range(len(unmatched_payout_list)):
    sheet.append(["", unmatched_payout_list[i], unmatched_utr_list[i], unmatched_amount_list[i], unmatched_cid_list[i],unmatched_cname_list[i], unmatched_created_at_list_formatted[i] ])

workbook.save("razor_pay_ids.xlsx")

