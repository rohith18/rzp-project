import requests
import argparse


parser = argparse.ArgumentParser()

parser.add_argument("--month", type=str, required=True)
parser.add_argument("--day", type=str, required=True)

args = parser.parse_args()

# March 1st
base_time_stamp = 1646092800

from_date = base_time_stamp + (int(args.day) - 1)*86400
to_date = from_date + 86400

final_p_to_r_response_list, p_to_r_response_list, final_c_to_p_response_list, c_to_p_response_list = [[] for _ in range(4)]

c_to_p_url = "https://api.pharmacyone.io/prod/rzp_transaction"
p_to_r_url = "https://api.pharmacyone.io/prod/rzp_payout"

for skip in range(0, 100000, 100):
    payload = {"skip": skip, "from": from_date, "to": to_date}
    headers = {"session-token": "wantednote"}

    c_to_p_response = requests.get(c_to_p_url, params=payload, headers=headers)
    c_to_p_response_body = c_to_p_response.json()
    try:
        c_to_p_response_list = c_to_p_response_body.get("data").get("items")
    except AttributeError:
        pass
    final_c_to_p_response_list = final_c_to_p_response_list + c_to_p_response_list

    p_to_r_response = requests.get(p_to_r_url, params=payload, headers=headers)
    p_to_r_response_body = p_to_r_response.json()
    try:
        p_to_r_response_list = p_to_r_response_body.get("data").get("items")
    except AttributeError:
        pass
    final_p_to_r_response_list += p_to_r_response_list

    if (len(c_to_p_response_list) == 0 and len(p_to_r_response_list) == 0):
        break

